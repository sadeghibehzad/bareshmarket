package com.baran.bareshmarket.repository.api;

import com.baran.bareshmarket.models.category.api.CategoryResponse;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.models.product.api.SingleProductResponse;
import com.baran.bareshmarket.models.slider.api.SliderResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetroApi {
    String api_key = "TUlJQ0lqQU5CZ2txaGtpRzl3aE9nQnFLcEQrTEhGV3dIYVFkYVJpT3ROL3VmZ1hXZ2NlUFJQcVRuRUhPZVJvOFlPMllDb0JBeGQ3YVJsL2VDWEozMHYxbklLdzE0RjBzanlsY0paRFIwOGlhbGlyZXphdEw5WlpQNytIaFpSWFFNNlNRSWE5NkFOdjBuRmIwSDRhTWR3WmJlY01LdFBkYzNMcFFaUEo2elJnbXNZQ1U2QVdmaGpMWGhETDZyVlptdmovTGg2Q1dVY0E2cGRkUlJmTEVzS1laMEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUFxR21BZTFNaGVLM0h5bDRPQlE4dk41Uk5aY3ZhdWJ2c0crZ1hTeU52Vyt0WnFpZnROMUVBbmZ6WC93dzkvc1lGZ1hrbTFJU09PNVprM3Z5cnhnUUNPL2FiKzVJb1o4bWRvaGNmdDB0YjlJeVB5ODl2TTRmUENFVS1WVWNXOG5ZbHhKbjd5d1E4bTJxMGF5TnlBSDFLRDF4SDJaLWZRNkNpNzRwZC9iRDIyaWx1RGJDeDhWRXpIVXdaMnhXMm43U1FxaHoyWkliVXRTZ2k4bzFrK3F3YTMvWkxHbGF1TWl6byszZnRNejBTNDRDdXQ1NUpxRVBVb3UwL3IweVZocnNNRy9NY0U4dGg2UmxEWHppeGNuMzl1ellYOTFvaVRJakVJUE9lTXdtbytsNWlvN1htSWFHZXRjemRrU3V3UGVZenc2VUc4Z1dSdlRyREpYRi9KZUhYeTRabXFCajNZUkMxdC81R3hRQWJGZlVmUlVpNEs5cGxaOE91dFZ4aWFsaXJlemFyQjVXOFRPY3RwaUtpb3doZG8yNVhobHFWc1ZOcE1qeHVnR3VwVVk3bUpYdElRMjFHMDZjNWVhdTVnK3VYZXpPMGpwNW5qZ1ZNNHBBZ1RNQ1AxNk5KQm9hV093dlB6bUUvRE1xZlBCRUc0MFNiR2tJRUVDQXdFQUFR";

    @GET("categories")
    @Headers("api-access-key: " + api_key)
    Observable<CategoryResponse> getCategories();

    @GET("categories/{catId}")
    @Headers("api-access-key: " + api_key)
    Observable<CategoryResponse> getCategoryById(@Path("catId") int catId);

    @GET("category/{catId}/products")
    @Headers("api-access-key: " + api_key)
    Observable<ProductResponse> getCategoryProductsById(@Path("catId") int catId, @Query("page") int page);

    @GET("last-products")
    @Headers("api-access-key: " + api_key)
    Observable<ProductResponse> getLastProducts(@Query("page") int page);

    @GET("best-selling-products")
    @Headers("api-access-key: " + api_key)
    Observable<ProductResponse> getBestSellingProducts(@Query("page") int page);

    @GET("sliders")
    @Headers("api-access-key: " + api_key)
    Observable<SliderResponse> getSliders();

    @GET("product/{id}")
    @Headers("api-access-key: " + api_key)
    Observable<SingleProductResponse> getProductById(@Path("id") int id);
}
