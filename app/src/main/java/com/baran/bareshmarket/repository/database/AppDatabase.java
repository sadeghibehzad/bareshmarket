package com.baran.bareshmarket.repository.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.baran.bareshmarket.models.TempData;

@Database(entities = {
        TempData.class
}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TempDao tempDao();
}
