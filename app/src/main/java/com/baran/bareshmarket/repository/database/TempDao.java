package com.baran.bareshmarket.repository.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.baran.bareshmarket.models.TempData;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface TempDao {

    @Query("SELECT * FROM tbl_temp")
    Flowable<List<TempData>> getTempDatas();

    @Query("SELECT * FROM tbl_temp")
    TempData getMoviesNflow();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTemp(TempData data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTemps(List<TempData> data);

    @Query("DELETE FROM tbl_temp")
    void deleteAll();

    @Query("SELECT * FROM tbl_temp WHERE _id = :id")
    Flowable<TempData> getGenreById(int id);

}
