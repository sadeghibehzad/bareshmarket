package com.baran.bareshmarket;

import androidx.multidex.MultiDex;

import com.baran.bareshmarket.di.DaggerAppComponent;
import com.baran.bareshmarket.util.TypefaceUtil;
import com.orhanobut.hawk.Hawk;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class BaseApplication extends DaggerApplication {

    public static boolean splashShown = false;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        MultiDex.install(this);
        Hawk.init(getApplicationContext()).build();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/sahel.ttf");
        return DaggerAppComponent.builder().application(this).build();
    }
}
