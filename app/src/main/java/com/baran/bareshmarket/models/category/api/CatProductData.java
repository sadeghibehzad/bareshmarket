package com.baran.bareshmarket.models.category.api;

import com.baran.bareshmarket.models.product.api.Product;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatProductData {
    @SerializedName("data")
    private List<Product> data;

    public CatProductData(List<Product> data) {
        this.data = data;
    }

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }
}
