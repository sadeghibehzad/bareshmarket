package com.baran.bareshmarket.models;

import com.google.gson.annotations.SerializedName;

public class Pages {
    @SerializedName("currentPage")
    private int currentPage;
    @SerializedName("lastPage")
    private int lastPage;
    @SerializedName("total")
    private int total;

    public Pages(int currentPage, int lastPage, int total) {
        this.currentPage = currentPage;
        this.lastPage = lastPage;
        this.total = total;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
