package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

public class ProductAttribute {
    @SerializedName("attribute_group_name")
    private String attribute_group_name;
    @SerializedName("attribute_title_id")
    private int attribute_title_id;
    @SerializedName("attribute_title")
    private String attribute_title;
    @SerializedName("attribute_value_id")
    private int attribute_value_id;
    @SerializedName("attribute_value")
    private String attribute_value;

    public ProductAttribute(String attribute_group_name, int attribute_title_id, String attribute_title, int attribute_value_id, String attribute_value) {
        this.attribute_group_name = attribute_group_name;
        this.attribute_title_id = attribute_title_id;
        this.attribute_title = attribute_title;
        this.attribute_value_id = attribute_value_id;
        this.attribute_value = attribute_value;
    }

    public String getAttribute_group_name() {
        return attribute_group_name;
    }

    public void setAttribute_group_name(String attribute_group_name) {
        this.attribute_group_name = attribute_group_name;
    }

    public int getAttribute_title_id() {
        return attribute_title_id;
    }

    public void setAttribute_title_id(int attribute_title_id) {
        this.attribute_title_id = attribute_title_id;
    }

    public String getAttribute_title() {
        return attribute_title;
    }

    public void setAttribute_title(String attribute_title) {
        this.attribute_title = attribute_title;
    }

    public int getAttribute_value_id() {
        return attribute_value_id;
    }

    public void setAttribute_value_id(int attribute_value_id) {
        this.attribute_value_id = attribute_value_id;
    }

    public String getAttribute_value() {
        return attribute_value;
    }

    public void setAttribute_value(String attribute_value) {
        this.attribute_value = attribute_value;
    }
}
