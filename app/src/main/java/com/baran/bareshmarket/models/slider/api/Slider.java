package com.baran.bareshmarket.models.slider.api;

import com.google.gson.annotations.SerializedName;

public class Slider {

    @SerializedName("slider_id")
    private int slider_id;
    @SerializedName("slider_title")
    private String slider_title;
    @SerializedName("slider_description")
    private String slider_description;
    @SerializedName("slider_web_address")
    private String slider_web_address;
    @SerializedName("slider_type")
    private String slider_type;
    @SerializedName("slider_type_id")
    private int slider_type_id;
    @SerializedName("slider_image")
    private String slider_image;
    @SerializedName("slider_image_resize")
    private String slider_image_resize;

    public Slider(int slider_id, String slider_title, String slider_description, String slider_web_address, String slider_type, int slider_type_id, String slider_image, String slider_image_resize) {
        this.slider_id = slider_id;
        this.slider_title = slider_title;
        this.slider_description = slider_description;
        this.slider_web_address = slider_web_address;
        this.slider_type = slider_type;
        this.slider_type_id = slider_type_id;
        this.slider_image = slider_image;
        this.slider_image_resize = slider_image_resize;
    }

    public int getSlider_id() {
        return slider_id;
    }

    public void setSlider_id(int slider_id) {
        this.slider_id = slider_id;
    }

    public String getSlider_title() {
        return slider_title;
    }

    public void setSlider_title(String slider_title) {
        this.slider_title = slider_title;
    }

    public String getSlider_description() {
        return slider_description;
    }

    public void setSlider_description(String slider_description) {
        this.slider_description = slider_description;
    }

    public String getSlider_web_address() {
        return slider_web_address;
    }

    public void setSlider_web_address(String slider_web_address) {
        this.slider_web_address = slider_web_address;
    }

    public String getSlider_type() {
        return slider_type;
    }

    public void setSlider_type(String slider_type) {
        this.slider_type = slider_type;
    }

    public int getSlider_type_id() {
        return slider_type_id;
    }

    public void setSlider_type_id(int slider_type_id) {
        this.slider_type_id = slider_type_id;
    }

    public String getSlider_image() {
        return slider_image;
    }

    public void setSlider_image(String slider_image) {
        this.slider_image = slider_image;
    }

    public String getSlider_image_resize() {
        return slider_image_resize;
    }

    public void setSlider_image_resize(String slider_image_resize) {
        this.slider_image_resize = slider_image_resize;
    }
}
