package com.baran.bareshmarket.models.mianpage;

import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.models.slider.api.SliderResponse;

import java.util.List;

public class ProductsCombinedRequest {
    private ProductResponse latestProducts;
    private ProductResponse bestSellings;
    private SliderResponse sliderResponse;

    public ProductsCombinedRequest(ProductResponse latestProducts, ProductResponse bestSellings, SliderResponse sliderResponse) {
        this.latestProducts = latestProducts;
        this.bestSellings = bestSellings;
        this.sliderResponse = sliderResponse;
    }

    public ProductResponse getLatestProducts() {
        return latestProducts;
    }

    public void setLatestProducts(ProductResponse latestProducts) {
        this.latestProducts = latestProducts;
    }

    public ProductResponse getBestSellings() {
        return bestSellings;
    }

    public void setBestSellings(ProductResponse bestSellings) {
        this.bestSellings = bestSellings;
    }

    public SliderResponse getSliderResponse() {
        return sliderResponse;
    }

    public void setSliderResponse(SliderResponse sliderResponse) {
        this.sliderResponse = sliderResponse;
    }
}
