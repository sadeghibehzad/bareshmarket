package com.baran.bareshmarket.models.category.api;

import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName("category_id")
    private int category_id;
    @SerializedName("category_parent_id")
    private int category_parent_id;
    @SerializedName("category_title")
    private String category_title;
    @SerializedName("category_slug")
    private String category_slug;
    @SerializedName("category_children")
    private CatChildData category_children;
    @SerializedName("category_products")
    private CatProductData category_products;

    public Category(int category_id, int category_parent_id, String category_title, String category_slug, CatChildData category_children, CatProductData category_products) {
        this.category_id = category_id;
        this.category_parent_id = category_parent_id;
        this.category_title = category_title;
        this.category_slug = category_slug;
        this.category_children = category_children;
        this.category_products = category_products;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getCategory_parent_id() {
        return category_parent_id;
    }

    public void setCategory_parent_id(int category_parent_id) {
        this.category_parent_id = category_parent_id;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public String getCategory_slug() {
        return category_slug;
    }

    public void setCategory_slug(String category_slug) {
        this.category_slug = category_slug;
    }

    public CatChildData getCategory_children() {
        return category_children;
    }

    public void setCategory_children(CatChildData category_children) {
        this.category_children = category_children;
    }

    public CatProductData getCategory_products() {
        return category_products;
    }

    public void setCategory_products(CatProductData category_products) {
        this.category_products = category_products;
    }
}
