package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

public class ProductSupplier {
    @SerializedName("supplier_name_fa")
    private String supplier_name_fa;
    @SerializedName("supplier_name_en")
    private String supplier_name_en;
    @SerializedName("supplier_allow_order")
    private boolean supplier_allow_order;
    @SerializedName("supplier_monthly_profit")
    private int supplier_monthly_profit;
    @SerializedName("supplier_pre_payment")
    private int supplier_pre_payment;
    @SerializedName("supplier_pre_payment_percentage")
    private int supplier_pre_payment_percentage;

    public ProductSupplier(String supplier_name_fa, String supplier_name_en, boolean supplier_allow_order, int supplier_monthly_profit, int supplier_pre_payment, int supplier_pre_payment_percentage) {
        this.supplier_name_fa = supplier_name_fa;
        this.supplier_name_en = supplier_name_en;
        this.supplier_allow_order = supplier_allow_order;
        this.supplier_monthly_profit = supplier_monthly_profit;
        this.supplier_pre_payment = supplier_pre_payment;
        this.supplier_pre_payment_percentage = supplier_pre_payment_percentage;
    }

    public String getSupplier_name_fa() {
        return supplier_name_fa;
    }

    public void setSupplier_name_fa(String supplier_name_fa) {
        this.supplier_name_fa = supplier_name_fa;
    }

    public String getSupplier_name_en() {
        return supplier_name_en;
    }

    public void setSupplier_name_en(String supplier_name_en) {
        this.supplier_name_en = supplier_name_en;
    }

    public boolean isSupplier_allow_order() {
        return supplier_allow_order;
    }

    public void setSupplier_allow_order(boolean supplier_allow_order) {
        this.supplier_allow_order = supplier_allow_order;
    }

    public int getSupplier_monthly_profit() {
        return supplier_monthly_profit;
    }

    public void setSupplier_monthly_profit(int supplier_monthly_profit) {
        this.supplier_monthly_profit = supplier_monthly_profit;
    }

    public int getSupplier_pre_payment() {
        return supplier_pre_payment;
    }

    public void setSupplier_pre_payment(int supplier_pre_payment) {
        this.supplier_pre_payment = supplier_pre_payment;
    }

    public int getSupplier_pre_payment_percentage() {
        return supplier_pre_payment_percentage;
    }

    public void setSupplier_pre_payment_percentage(int supplier_pre_payment_percentage) {
        this.supplier_pre_payment_percentage = supplier_pre_payment_percentage;
    }
}
