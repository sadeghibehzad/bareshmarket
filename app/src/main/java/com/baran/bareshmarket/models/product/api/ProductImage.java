package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

public class ProductImage {
    @SerializedName("image_id")
    private int image_id;
    @SerializedName("image_name")
    private String image_name;
    @SerializedName("image_path")
    private String image_path;
    @SerializedName("image_cover")
    private String image_cover;

    public ProductImage(int image_id, String image_name, String image_path, String image_cover) {
        this.image_id = image_id;
        this.image_name = image_name;
        this.image_path = image_path;
        this.image_cover = image_cover;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getImage_cover() {
        return image_cover;
    }

    public void setImage_cover(String image_cover) {
        this.image_cover = image_cover;
    }
}
