package com.baran.bareshmarket.models.slider.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SliderResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<Slider> sliders;

    public SliderResponse(String status, String message, List<Slider> sliders) {
        this.status = status;
        this.message = message;
        this.sliders = sliders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Slider> getSliders() {
        return sliders;
    }

    public void setSliders(List<Slider> sliders) {
        this.sliders = sliders;
    }
}
