package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

public class ProductCategory {
    @SerializedName("category_id")
    private int category_id;
    @SerializedName("category_title")
    private String category_title;
    @SerializedName("category_slug")
    private String category_slug;
    @SerializedName("category_icon")
    private String category_icon;
    @SerializedName("category_status")
    private String category_status;
    @SerializedName("category_parent_id")
    private int category_parent_id;

    public ProductCategory(int category_id, String category_title, String category_slug, String category_icon, String category_status, int category_parent_id) {
        this.category_id = category_id;
        this.category_title = category_title;
        this.category_slug = category_slug;
        this.category_icon = category_icon;
        this.category_status = category_status;
        this.category_parent_id = category_parent_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public String getCategory_slug() {
        return category_slug;
    }

    public void setCategory_slug(String category_slug) {
        this.category_slug = category_slug;
    }

    public String getCategory_icon() {
        return category_icon;
    }

    public void setCategory_icon(String category_icon) {
        this.category_icon = category_icon;
    }

    public String getCategory_status() {
        return category_status;
    }

    public void setCategory_status(String category_status) {
        this.category_status = category_status;
    }

    public int getCategory_parent_id() {
        return category_parent_id;
    }

    public void setCategory_parent_id(int category_parent_id) {
        this.category_parent_id = category_parent_id;
    }
}
