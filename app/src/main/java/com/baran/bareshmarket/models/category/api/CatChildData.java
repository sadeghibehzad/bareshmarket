package com.baran.bareshmarket.models.category.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatChildData {
    @SerializedName("data")
    private List<Category> data;

    public CatChildData(List<Category> data) {
        this.data = data;
    }

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }
}
