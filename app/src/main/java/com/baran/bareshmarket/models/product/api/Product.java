package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {
    @SerializedName("product_id")
    private int product_id;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("product_short_description")
    private String product_short_description;
    @SerializedName("product_description")
    private String product_description;
    @SerializedName("product_status")
    private boolean product_status;
    @SerializedName("product_is_exist")
    private boolean product_is_exist;
    @SerializedName("product_allow_order")
    private boolean product_allow_order;
    @SerializedName("product_url")
    private String product_url;
    @SerializedName("product_quantity")
    private int product_quantity;
    @SerializedName("product_price")
    private int product_price;
    @SerializedName("product_installment_count")
    private int product_installment_count;
    @SerializedName("product_supplier")
    private ProductSupplier product_supplier;
    @SerializedName("product_tag")
    private List<ProductTag> product_tag;
    @SerializedName("product_category")
    private List<ProductCategory> product_category;
    @SerializedName("product_images")
    private List<ProductImage> product_images;
    @SerializedName("product_attributes")
    private List<ProductAttribute> product_attributes;

    public Product(int product_id, String product_name, String product_short_description, String product_description, boolean product_status, boolean product_is_exist, boolean product_allow_order, String product_url, int product_quantity, int product_price, int product_installment_count, ProductSupplier product_supplier, List<ProductTag> product_tag, List<ProductCategory> product_category, List<ProductImage> product_images, List<ProductAttribute> product_attributes) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_short_description = product_short_description;
        this.product_description = product_description;
        this.product_status = product_status;
        this.product_is_exist = product_is_exist;
        this.product_allow_order = product_allow_order;
        this.product_url = product_url;
        this.product_quantity = product_quantity;
        this.product_price = product_price;
        this.product_installment_count = product_installment_count;
        this.product_supplier = product_supplier;
        this.product_tag = product_tag;
        this.product_category = product_category;
        this.product_images = product_images;
        this.product_attributes = product_attributes;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_short_description() {
        return product_short_description;
    }

    public void setProduct_short_description(String product_short_description) {
        this.product_short_description = product_short_description;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public boolean isProduct_status() {
        return product_status;
    }

    public void setProduct_status(boolean product_status) {
        this.product_status = product_status;
    }

    public boolean isProduct_is_exist() {
        return product_is_exist;
    }

    public void setProduct_is_exist(boolean product_is_exist) {
        this.product_is_exist = product_is_exist;
    }

    public boolean isProduct_allow_order() {
        return product_allow_order;
    }

    public void setProduct_allow_order(boolean product_allow_order) {
        this.product_allow_order = product_allow_order;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public int getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(int product_quantity) {
        this.product_quantity = product_quantity;
    }

    public int getProduct_price() {
        return product_price;
    }

    public void setProduct_price(int product_price) {
        this.product_price = product_price;
    }

    public int getProduct_installment_count() {
        return product_installment_count;
    }

    public void setProduct_installment_count(int product_installment_count) {
        this.product_installment_count = product_installment_count;
    }

    public ProductSupplier getProduct_supplier() {
        return product_supplier;
    }

    public void setProduct_supplier(ProductSupplier product_supplier) {
        this.product_supplier = product_supplier;
    }

    public List<ProductTag> getProduct_tag() {
        return product_tag;
    }

    public void setProduct_tag(List<ProductTag> product_tag) {
        this.product_tag = product_tag;
    }

    public List<ProductCategory> getProduct_category() {
        return product_category;
    }

    public void setProduct_category(List<ProductCategory> product_category) {
        this.product_category = product_category;
    }

    public List<ProductImage> getProduct_images() {
        return product_images;
    }

    public void setProduct_images(List<ProductImage> product_images) {
        this.product_images = product_images;
    }

    public List<ProductAttribute> getProduct_attributes() {
        return product_attributes;
    }

    public void setProduct_attributes(List<ProductAttribute> product_attributes) {
        this.product_attributes = product_attributes;
    }
}
