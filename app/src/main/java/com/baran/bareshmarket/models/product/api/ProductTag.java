package com.baran.bareshmarket.models.product.api;

import com.google.gson.annotations.SerializedName;

public class ProductTag {
    @SerializedName("tag_id")
    private int tag_id;
    @SerializedName("tag_title")
    private String tag_title;
    @SerializedName("tag_slug")
    private String tag_slug;

    public ProductTag(int tag_id, String tag_title, String tag_slug) {
        this.tag_id = tag_id;
        this.tag_title = tag_title;
        this.tag_slug = tag_slug;
    }

    public int getTag_id() {
        return tag_id;
    }

    public void setTag_id(int tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag_title() {
        return tag_title;
    }

    public void setTag_title(String tag_title) {
        this.tag_title = tag_title;
    }

    public String getTag_slug() {
        return tag_slug;
    }

    public void setTag_slug(String tag_slug) {
        this.tag_slug = tag_slug;
    }
}
