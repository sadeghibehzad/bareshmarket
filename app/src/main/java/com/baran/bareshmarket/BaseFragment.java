package com.baran.bareshmarket;

import android.os.Bundle;
import androidx.annotation.Nullable;
import dagger.android.support.DaggerFragment;

public class BaseFragment extends DaggerFragment {

    public BaseFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
