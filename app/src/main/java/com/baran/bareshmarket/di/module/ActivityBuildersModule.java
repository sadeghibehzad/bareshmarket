package com.baran.bareshmarket.di.module;

import com.baran.bareshmarket.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = {MainViewModelsModule.class, FragmentBuildersModule.class})
    abstract MainActivity contributeMainActivity();
}
