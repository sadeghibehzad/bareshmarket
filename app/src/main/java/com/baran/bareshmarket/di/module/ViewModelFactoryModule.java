package com.baran.bareshmarket.di.module;

import androidx.lifecycle.ViewModelProvider;

import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelFactory);

}