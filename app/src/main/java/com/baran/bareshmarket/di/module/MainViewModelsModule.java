package com.baran.bareshmarket.di.module;

import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.di.ViewModelKey;
import com.baran.bareshmarket.ui.basketfrag.BasketViewModel;
import com.baran.bareshmarket.ui.categoriesfrag.CategoriesViewModel;
import com.baran.bareshmarket.ui.main.MainActViewModel;
import com.baran.bareshmarket.ui.mainfrag.MainFragViewModel;
import com.baran.bareshmarket.ui.mainfragloadmore.LoadMoreViewModel;
import com.baran.bareshmarket.ui.productfrag.ProductViewModel;
import com.baran.bareshmarket.ui.profilefrag.ProfileViewModel;
import com.baran.bareshmarket.ui.settingfrag.SettingViewModel;
import com.baran.bareshmarket.ui.signupfrag.confrimfrag.ConfirmViewModel;
import com.baran.bareshmarket.ui.signupfrag.phonefrag.PhoneViewModel;
import com.baran.bareshmarket.ui.singlecategoryfrag.SingleCatViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActViewModel.class)
    public abstract ViewModel bindMainActivityViewModel(MainActViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainFragViewModel.class)
    public abstract ViewModel bindMainFragViewModel(MainFragViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CategoriesViewModel.class)
    public abstract ViewModel bindCategoriesViewModel(CategoriesViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BasketViewModel.class)
    public abstract ViewModel bindBasketViewModel(BasketViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel.class)
    public abstract ViewModel bindProfileViewModel(ProfileViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SettingViewModel.class)
    public abstract ViewModel bindSettingViewModel(SettingViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SingleCatViewModel.class)
    public abstract ViewModel bindSingleCatViewModel(SingleCatViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PhoneViewModel.class)
    public abstract ViewModel bindPhoneViewModel(PhoneViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ConfirmViewModel.class)
    public abstract ViewModel bindConfirmViewModel(ConfirmViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoadMoreViewModel.class)
    public abstract ViewModel bindLoadMoreViewModel(LoadMoreViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel.class)
    public abstract ViewModel bindProductViewModel(ProductViewModel viewModel);

}