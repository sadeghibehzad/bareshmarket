package com.baran.bareshmarket.di;

import android.app.Application;

import com.baran.bareshmarket.BaseApplication;
import com.baran.bareshmarket.di.module.ActivityBuildersModule;
import com.baran.bareshmarket.di.module.AppModule;
import com.baran.bareshmarket.di.module.DatabaseModule;
import com.baran.bareshmarket.di.module.ViewModelFactoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuildersModule.class,
                ViewModelFactoryModule.class,
                AppModule.class,
                DatabaseModule.class
        }
)
public interface AppComponent extends AndroidInjector<BaseApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
