package com.baran.bareshmarket.di.module;

import com.baran.bareshmarket.ui.basketfrag.BasketFragment;
import com.baran.bareshmarket.ui.categoriesfrag.CategoriesFragment;
import com.baran.bareshmarket.ui.mainfrag.MainFragment;
import com.baran.bareshmarket.ui.mainfragloadmore.LoadMoreProductFragment;
import com.baran.bareshmarket.ui.productfrag.ProductFragment;
import com.baran.bareshmarket.ui.profilefrag.ProfileFragment;
import com.baran.bareshmarket.ui.settingfrag.SettingFragment;
import com.baran.bareshmarket.ui.signupfrag.confrimfrag.ConfirmFragment;
import com.baran.bareshmarket.ui.signupfrag.phonefrag.PhoneFragment;
import com.baran.bareshmarket.ui.singlecategoryfrag.SingleCategoryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();

    @ContributesAndroidInjector
    abstract CategoriesFragment contributeCategoriesFragment();

    @ContributesAndroidInjector
    abstract BasketFragment contributeBasketFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract SettingFragment contributeSettingFragment();

    @ContributesAndroidInjector
    abstract SingleCategoryFragment contributeSingleCatFragment();

    @ContributesAndroidInjector
    abstract PhoneFragment contributePhoneFragment();

    @ContributesAndroidInjector
    abstract ConfirmFragment contributeConfirmFragment();

    @ContributesAndroidInjector
    abstract LoadMoreProductFragment contributeLoadMoreFragment();

    @ContributesAndroidInjector
    abstract ProductFragment contributeProductFragment();
}
