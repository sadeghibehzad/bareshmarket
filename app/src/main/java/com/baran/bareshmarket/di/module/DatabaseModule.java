package com.baran.bareshmarket.di.module;

import android.app.Application;

import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.baran.bareshmarket.repository.database.AppDatabase;
import com.baran.bareshmarket.repository.database.TempDao;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    private final String dBName = "BareshDatabase.db";

    @Provides
    @Singleton
    AppDatabase provideDatabase(@NonNull Application application) {
        return Room.databaseBuilder(application,
                AppDatabase.class, dBName)
                .allowMainThreadQueries().build();
    }

    @Singleton
    @Provides
    static TempDao providesTempDao(AppDatabase appDatabase) {
        return appDatabase.tempDao();
    }
}
