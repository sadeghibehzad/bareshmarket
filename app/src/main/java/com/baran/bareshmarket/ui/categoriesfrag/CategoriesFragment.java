package com.baran.bareshmarket.ui.categoriesfrag;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.adapters.categories.CategoriesAdapter;
import com.baran.bareshmarket.databinding.FragmentCategoriesBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.util.Resource;

import java.util.ArrayList;

import javax.inject.Inject;

public class CategoriesFragment extends BaseFragment {

    private static final String TAG = "CategoriesFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    @Inject
    CategoriesAdapter categoriesAdapter;
    private FragmentCategoriesBinding binding;
    private CategoriesViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false);
        binding.setLoading(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this, providerFactory).get(CategoriesViewModel.class);
        setupRecycler();
        observers();

        binding.swipeRefresh.setOnRefreshListener(() -> {
            if (binding.swipeRefresh.isRefreshing()) {
                viewModel.getCategories();
                categoriesAdapter.setData(new ArrayList<>());
            }
        });

        categoriesAdapter.setShowMoreListener((position, category) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("cat_id", category.getCategory_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_categoriesFragment_to_singleCategoryFragment, bundle);
        });
    }

    void observers() {
        viewModel.getCategoryResults().observe(getViewLifecycleOwner(), listResource -> {
            binding.swipeRefresh.setRefreshing(false);
            if (listResource.status == Resource.Status.LOADING)
                binding.setLoading(true);
            else if (listResource.status == Resource.Status.SUCCESS || listResource.status == Resource.Status.ERROR) {
                binding.setLoading(false);
                categoriesAdapter.setData(listResource.data);
            }
        });
    }

    void setupRecycler() {
        binding.categories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.categories.setAdapter(categoriesAdapter);
    }
}