package com.baran.bareshmarket.ui.mainfrag;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.baran.bareshmarket.BaseApplication;
import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.adapters.categories.CategoriesProductAdapter;
import com.baran.bareshmarket.adapters.slider.SliderAdapter;
import com.baran.bareshmarket.databinding.FragmentMainBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.slider.api.Slider;
import com.baran.bareshmarket.ui.main.MainActivity;
import com.baran.bareshmarket.util.Resource;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;

import javax.inject.Inject;

public class MainFragment extends BaseFragment {

    private static final String TAG = "MainFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    private FragmentMainBinding binding;
    private MainFragViewModel viewModel;
    @Inject
    CategoriesProductAdapter latestAdapter;
    @Inject
    CategoriesProductAdapter bestSellingAdapter;
    @Inject
    SliderAdapter sliderAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this, providerFactory).get(MainFragViewModel.class);
        setupRecycler();
        observers();
        binding.setVisibleItems(false);
        binding.setLoading(false);
        ((MainActivity) getActivity()).setSplashVisibility(!BaseApplication.splashShown);
        binding.catMore.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("type", "latest");
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_loadMoreProductFragment, bundle);
        });

        binding.catMore2.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("type", "bestSellers");
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_loadMoreProductFragment, bundle);
        });

        ((MainActivity) getActivity()).getReload().setOnClickListener(v -> {
            try {
                ((MainActivity) getActivity()).setReloadVisibility(false);
            } catch (Exception e) {
            }
            viewModel.getData();
        });

        sliderAdapter.setListener((slider, position) -> {
            Bundle bundle = new Bundle();
            switch (slider.getSlider_type()){
                case "product":
                        bundle.putInt("product_id", slider.getSlider_type_id());
                        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_productFragment, bundle);
                    break;
                case "banner":
                        bundle.putString("address", slider.getSlider_image_resize());
                        bundle.putString("text", slider.getSlider_description());
                        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_bannerFragment, bundle);
                    break;
                case "web-url":
                        bundle.putString("address", slider.getSlider_web_address());
                        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_webViewFragment, bundle);
                    break;
            }
        });

        latestAdapter.setOnProductClickListener((product, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("product_id", product.getProduct_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_productFragment, bundle);
        });

        bestSellingAdapter.setOnProductClickListener((product, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("product_id", product.getProduct_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_mainFragment_to_productFragment, bundle);
        });
    }

    void observers() {
        viewModel.getMainRequests().observe(getViewLifecycleOwner(), productsCombinedRequestResource -> {
            if (productsCombinedRequestResource.status == Resource.Status.LOADING)
                binding.setLoading(true);
            if (productsCombinedRequestResource.status == Resource.Status.SUCCESS) {
                latestAdapter.setData(productsCombinedRequestResource.data.getLatestProducts().getData());
                bestSellingAdapter.setData(productsCombinedRequestResource.data.getBestSellings().getData());
                sliderAdapter.renewItems(productsCombinedRequestResource.data.getSliderResponse().getSliders());
                binding.setVisibleItems(true);
                binding.setLoading(false);
                new Handler().postDelayed(() -> {
                    try {
                        BaseApplication.splashShown = true;
                        ((MainActivity) getActivity()).setSplashVisibility(false);
                    } catch (Exception e) {
                    }
                }, 2500);
            } else if (productsCombinedRequestResource.status == Resource.Status.ERROR) {
                binding.setVisibleItems(false);
                binding.setLoading(false);
                try {
                    ((MainActivity) getActivity()).setSplashVisibility(true);
                    ((MainActivity) getActivity()).setReloadVisibility(true);
                    BaseApplication.splashShown = false;
                } catch (Exception e) {
                }
            }
        });
    }

    void setupRecycler() {
        binding.latest.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.latest.setAdapter(latestAdapter);

        binding.bestSellers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.bestSellers.setAdapter(bestSellingAdapter);

        binding.imageSlider.setSliderAdapter(sliderAdapter);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setScrollTimeInMillis(5000);
        binding.imageSlider.startAutoCycle();
    }
}