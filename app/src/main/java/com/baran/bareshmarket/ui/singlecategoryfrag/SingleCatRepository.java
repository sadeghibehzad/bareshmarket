package com.baran.bareshmarket.ui.singlecategoryfrag;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.models.category.api.CategoryResponse;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SingleCatRepository {

    private RetroApi retroApi;
    private MutableLiveData<Resource<Category>> categoryResults = new MutableLiveData<>();
    private MutableLiveData<Resource<ProductResponse>> catProductResults = new MutableLiveData<>();

    @Inject
    public SingleCatRepository(RetroApi retroApi) {
        this.retroApi = retroApi;
    }

    void getCatById(int catId){
        retroApi.getCategoryById(catId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        categoryResults.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {
                        if (categoryResponse.getData() != null)
                            categoryResults.setValue(Resource.success(categoryResponse.getData().get(0),categoryResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        categoryResults.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void getCatProductById(int catId, int page){
        retroApi.getCategoryProductsById(catId, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        catProductResults.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull ProductResponse productResponse) {
                        if (productResponse.getData() != null)
                            catProductResults.setValue(Resource.success(productResponse, productResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        catProductResults.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public LiveData<Resource<Category>> getCategoryResults() {
        return categoryResults;
    }

    public MutableLiveData<Resource<ProductResponse>> getCatProductResults() {
        return catProductResults;
    }
}
