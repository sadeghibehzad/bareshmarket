package com.baran.bareshmarket.ui.mainfragloadmore;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.util.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class LoadMoreViewModel extends ViewModel {

    private final LoadMoreRepository repository;
    private LiveData<Resource<ProductResponse>> productsResult = new MutableLiveData<>();
    private MutableLiveData<List<Product>> products = new MutableLiveData<>();
    private MutableLiveData<Integer> currentPage = new MutableLiveData<>();
    private MutableLiveData<Integer> totalPage = new MutableLiveData<>();

    @Inject
    public LoadMoreViewModel(LoadMoreRepository repository) {
        this.repository = repository;
        this.productsResult = repository.getProductsResult();
        this.products.setValue(new ArrayList<>());
    }

    void getLatestProducts(int page) {
        repository.getLatestProducts(page);
    }

    void getBestSellerProducts(int page) {
        repository.getBestSellerProducts(page);
    }

    public MutableLiveData<Integer> getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage.setValue(currentPage);
    }

    public MutableLiveData<Integer> getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage.setValue(totalPage);
    }

    public LiveData<Resource<ProductResponse>> getProductsResult() {
        return productsResult;
    }

    public MutableLiveData<List<Product>> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        if (!this.products.getValue().containsAll(products)){
            List<Product> temp = this.products.getValue();
            temp.addAll(products);
            this.products.setValue(temp);
        }
    }
}
