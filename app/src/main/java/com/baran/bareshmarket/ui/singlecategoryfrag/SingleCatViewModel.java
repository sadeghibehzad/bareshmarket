package com.baran.bareshmarket.ui.singlecategoryfrag;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.models.category.api.CategoryResponse;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SingleCatViewModel extends ViewModel {

    private SingleCatRepository repository;
    private LiveData<Resource<Category>> categoryResults = new MutableLiveData<>();
    private LiveData<Resource<ProductResponse>> catProductResults = new MutableLiveData<>();
    private Integer catId;

    @Inject
    public SingleCatViewModel(SingleCatRepository repository) {
        this.repository = repository;
        this.categoryResults = repository.getCategoryResults();
        this.catProductResults = repository.getCatProductResults();
        this.catId = 0;
    }

    void getCatById(int catId){
        repository.getCatById(catId);
    }

    void getCatProductById(int catId, int page){
        repository.getCatProductById(catId, page);
    }

    public LiveData<Resource<Category>> getCategoryResults() {
        return categoryResults;
    }

    public LiveData<Resource<ProductResponse>> getCatProductResults() {
        return catProductResults;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }
}
