package com.baran.bareshmarket.ui.sliderfrags.bannerfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.FragmentBannerBinding;
import com.bumptech.glide.Glide;

public class BannerFragment extends Fragment {

    private FragmentBannerBinding binding;
    private String address = "https://shop.barannet.ir/";
    private String text = "";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        address = getArguments().getString("address");
        text = getArguments().getString("text");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_banner, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Glide.with(binding.image).load(address).into(binding.image);
        binding.desc.setText(Html.fromHtml(text));
    }
}