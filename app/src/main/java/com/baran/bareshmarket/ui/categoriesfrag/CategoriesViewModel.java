package com.baran.bareshmarket.ui.categoriesfrag;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.util.Resource;

import java.util.List;

import javax.inject.Inject;

public class CategoriesViewModel extends ViewModel {

    private static final String TAG = "CategoriesViewModel";
    private CategoriesRepository categoriesRepository;
    private LiveData<Resource<List<Category>>> categoryResults;

    @Inject
    public CategoriesViewModel(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
        categoriesRepository.getCategories();
        this.categoryResults = categoriesRepository.getCategoryResults();
    }

    void getCategories() {
        categoriesRepository.getCategories();
    }

    public LiveData<Resource<List<Category>>> getCategoryResults() {
        return categoryResults;
    }
}
