package com.baran.bareshmarket.ui.signupfrag.phonefrag;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.FragmentPhoneBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;

import javax.inject.Inject;

public class PhoneFragment extends BaseFragment {

    private static final String TAG = "PhoneFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    private FragmentPhoneBinding binding;
    private PhoneViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this, providerFactory).get(PhoneViewModel.class);
        binding.btnSignUp.setOnClickListener(v ->
                Navigation.findNavController(binding.getRoot())
                .navigate(R.id.action_phoneFragment_to_confirmFragment));
    }
}