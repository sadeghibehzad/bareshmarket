package com.baran.bareshmarket.ui.mainfrag;

import androidx.lifecycle.MutableLiveData;

import com.baran.bareshmarket.models.mianpage.ProductsCombinedRequest;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;


import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainFragRepository {

    private MutableLiveData<Resource<ProductsCombinedRequest>> mainRequests = new MutableLiveData<>();
    private RetroApi retroApi;

    @Inject
    public MainFragRepository(RetroApi retroApi) {
        this.retroApi = retroApi;
    }

    void getLatestAndBest(){
        Observable.zip(
                retroApi.getLastProducts(1),
                retroApi.getBestSellingProducts(1),
                retroApi.getSliders(),
                ProductsCombinedRequest::new
        )
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<ProductsCombinedRequest>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mainRequests.postValue(Resource.loading(null));
            }

            @Override
            public void onNext(@NonNull ProductsCombinedRequest productsCombinedRequest) {
                mainRequests.postValue(Resource.success(productsCombinedRequest, productsCombinedRequest.getLatestProducts().getMessage()));
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mainRequests.postValue(Resource.error(e.getMessage(), null));
            }

            @Override
            public void onComplete() {

            }
        });

    }

    public MutableLiveData<Resource<ProductsCombinedRequest>> getMainRequests() {
        return mainRequests;
    }
}
