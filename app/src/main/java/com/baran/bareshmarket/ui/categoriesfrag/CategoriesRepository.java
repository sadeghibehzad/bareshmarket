package com.baran.bareshmarket.ui.categoriesfrag;

import androidx.lifecycle.MutableLiveData;

import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.models.category.api.CategoryResponse;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CategoriesRepository {

    private RetroApi retroApi;
    private MutableLiveData<Resource<List<Category>>> categoryResults = new MutableLiveData<>();

    @Inject
    public CategoriesRepository(RetroApi retroApi) {
        this.retroApi = retroApi;
    }

    void getCategories() {
        retroApi.getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        categoryResults.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {
                        if (categoryResponse.getStatus().toLowerCase().equals("ok"))
                            categoryResults.setValue(Resource.success(categoryResponse.getData(), categoryResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        categoryResults.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public MutableLiveData<Resource<List<Category>>> getCategoryResults() {
        return categoryResults;
    }
}
