package com.baran.bareshmarket.ui.productfrag;

import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.util.Resource;

import javax.inject.Inject;

public class ProductViewModel extends ViewModel {

    private static final String TAG = "ProductViewModel";
    private ProductRepository repository;
    private LiveData<Resource<Product>> productResult = new MutableLiveData<>();
    private MutableLiveData<Integer> product_id = new MutableLiveData<>();
    @Inject
    public ProductViewModel(ProductRepository repository) {
        this.repository = repository;
        this.productResult = repository.getProductResult();
        this.product_id.setValue(0);
    }

    void getProductById(int id) {
        repository.getProductById(id);
    }

    public MutableLiveData<Integer> getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id.setValue(product_id);
    }

    public LiveData<Resource<Product>> getProductResult() {
        return productResult;
    }
}
