package com.baran.bareshmarket.ui.profilefrag;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.FragmentProfileBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;

import javax.inject.Inject;

public class ProfileFragment extends BaseFragment {

    private static final String TAG = "ProfileFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    private FragmentProfileBinding binding;
    private ProfileViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this,providerFactory).get(ProfileViewModel.class);
        binding.btnSignUp.setOnClickListener(v ->
                Navigation.findNavController(binding.getRoot()).navigate(R.id.action_profileFragment_to_signup_navigation));
    }
}