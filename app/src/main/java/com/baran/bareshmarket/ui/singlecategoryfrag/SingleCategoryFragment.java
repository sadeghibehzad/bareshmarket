package com.baran.bareshmarket.ui.singlecategoryfrag;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.adapters.categories.SingleCategoryAdapter;
import com.baran.bareshmarket.adapters.categories.SingleCategoryProductsAdapter;
import com.baran.bareshmarket.databinding.FragmentSingleCategoryBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.ui.main.MainActivity;
import com.baran.bareshmarket.util.Resource;

import javax.inject.Inject;

public class SingleCategoryFragment extends BaseFragment {

    private static final String TAG = "SingleCategoryFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    @Inject
    SingleCategoryAdapter singleCategoryAdapter;
    @Inject
    SingleCategoryProductsAdapter singleCategoryProductsAdapter;

    private FragmentSingleCategoryBinding binding;
    private SingleCatViewModel viewModel;
    private int cat_id = 1;
    private int currentPage = 2;
    private int totalPage = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cat_id = getArguments().getInt("cat_id");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_single_category, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, providerFactory).get(SingleCatViewModel.class);
        if (viewModel.getCatId() == 0) {
            viewModel.setCatId(cat_id);
            viewModel.getCatById(cat_id);
        }
        binding.setLoading(false);
        setupRecycler();
        observers();
        singleCategoryAdapter.setCatListener((position, category) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("cat_id", category.getCategory_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_singleCategoryFragment_self, bundle);
        });

        singleCategoryProductsAdapter.setOnProductClickListener((product, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("product_id", product.getProduct_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_singleCategoryFragment_to_productFragment, bundle);
        });

        binding.scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            try {
                View view1 = (View) binding.scrollView.getChildAt(binding.scrollView.getChildCount() - 1);
                int diff = (view1.getBottom() - (binding.scrollView.getHeight() + binding.scrollView.getScrollY()));
                if (diff == 0)
                    if (currentPage <= totalPage) {
                        binding.setLoadMore(true);
                        viewModel.getCatProductById(cat_id, currentPage);
                    } else
                        Toast.makeText(getContext(), "داده ای برای بارگذاری موجود نیست", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
            }
        });
    }

    void observers() {
        if (!viewModel.getCategoryResults().hasActiveObservers()) {
            viewModel.getCategoryResults().observe(getViewLifecycleOwner(), listResource -> {
                if (listResource.status == Resource.Status.LOADING) {
                    binding.setLoading(true);
                } else if (listResource.status == Resource.Status.SUCCESS) {
                    binding.setLoading(false);
                    binding.subCatText.setText("دسته های : " + listResource.data.getCategory_title());
                    ((MainActivity) getActivity()).setSearchHint("جستجو در دسته " + listResource.data.getCategory_title() + " ....");
                    binding.subCatProductsText.setText("محصولات : " + listResource.data.getCategory_title());
                    if (listResource.data.getCategory_children().getData().size() == 0) {
                        binding.subCatText.setVisibility(View.GONE);
                        binding.categories.setVisibility(View.GONE);
                    }
                    singleCategoryAdapter.setData(listResource.data.getCategory_children().getData());
                    singleCategoryProductsAdapter.setData(listResource.data.getCategory_products().getData());
                } else if (listResource.status == Resource.Status.ERROR) {
                    binding.setLoading(false);
                }
            });
        }

        if (!viewModel.getCatProductResults().hasActiveObservers()) {
            viewModel.getCatProductResults().observe(getViewLifecycleOwner(), productResponseResource -> {
                if (productResponseResource.status == Resource.Status.LOADING)
                    binding.setLoadMore(true);
                else if (productResponseResource.status == Resource.Status.SUCCESS) {
                    binding.setLoadMore(false);
                    if (productResponseResource.data.getData() != null) {
                        currentPage++;
                        totalPage = productResponseResource.data.getPages().getLastPage();
                        singleCategoryProductsAdapter.addProducts(productResponseResource.data.getData());
                        singleCategoryProductsAdapter.notifyDataSetChanged();
                    }
                } else binding.setLoadMore(false);
            });
        }
    }

    void setupRecycler() {
        binding.categories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.categories.setAdapter(singleCategoryAdapter);

        binding.catProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.catProducts.setAdapter(singleCategoryProductsAdapter);
    }
}