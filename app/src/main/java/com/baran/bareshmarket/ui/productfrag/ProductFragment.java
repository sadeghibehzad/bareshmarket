package com.baran.bareshmarket.ui.productfrag;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.adapters.product.ProductImagesAdapter;
import com.baran.bareshmarket.adapters.slider.SliderAdapter;
import com.baran.bareshmarket.databinding.AttributesLayoutHeaderBinding;
import com.baran.bareshmarket.databinding.AttributesLayoutItemBinding;
import com.baran.bareshmarket.databinding.FragmentProductBinding;
import com.baran.bareshmarket.databinding.ProductTagsLayoutBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductAttribute;
import com.baran.bareshmarket.models.product.api.ProductImage;
import com.baran.bareshmarket.models.product.api.ProductTag;
import com.baran.bareshmarket.models.slider.api.Slider;
import com.baran.bareshmarket.util.Resource;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.IndicatorView.draw.data.RtlMode;
import com.smarteist.autoimageslider.SliderAnimations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class ProductFragment extends BaseFragment {

    private static final String TAG = "ProductFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    @Inject
    ProductImagesAdapter adapter;
    @Inject
    SliderAdapter sliderAdapter;

    private FragmentProductBinding binding;
    private ProductViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, providerFactory).get(ProductViewModel.class);
        if (viewModel.getProduct_id().getValue() == 0){
            viewModel.setProduct_id(getArguments().getInt("product_id", 1));
            viewModel.getProductById(viewModel.getProduct_id().getValue());
        }
        binding.setLoading(false);
        setupRecycler();
        observers();
    }

    void observers() {
        if (!viewModel.getProductResult().hasActiveObservers()) {
            viewModel.getProductResult().observe(getViewLifecycleOwner(), productResource -> {
                if (productResource.status == Resource.Status.LOADING)
                    binding.setLoading(true);
                else if (productResource.status == Resource.Status.SUCCESS) {
                    binding.setLoading(false);
                    binding.setProduct(productResource.data);
                    adapter.setData(productResource.data.getProduct_images());
                    List<Slider> sliderItems = new ArrayList<>();
                    for (ProductImage image : productResource.data.getProduct_images())
                        sliderItems.add(new Slider(image.getImage_id(), image.getImage_name(), image.getImage_path(),
                                image.getImage_path(), image.getImage_path(), 0, image.getImage_path(), image.getImage_path()));
                    sliderAdapter.renewItems(sliderItems);
                    try {
                        invisibleContainers(productResource.data);
                        for (ProductTag tag : productResource.data.getProduct_tag()) {
                            ProductTagsLayoutBinding bindingTag = DataBindingUtil.inflate(getLayoutInflater(), R.layout.product_tags_layout, null, false);
                            bindingTag.setTags(tag);
                            binding.tags.addView(bindingTag.getRoot());
                        }
                        HashMap<String, List<ProductAttribute>> attrs = new HashMap<>();
                        for (ProductAttribute row : productResource.data.getProduct_attributes()) {
                            List<ProductAttribute> temp = attrs.get(row.getAttribute_group_name()) == null ? new ArrayList<>() : attrs.get(row.getAttribute_group_name());
                            temp.add(row);
                            attrs.put(row.getAttribute_group_name(), temp);
                        }
                        for (Map.Entry row : attrs.entrySet()) {
                            AttributesLayoutHeaderBinding bindingHeader = DataBindingUtil.inflate(getLayoutInflater(), R.layout.attributes_layout_header, null, false);
                            bindingHeader.setAttribute(row.getKey().toString());
                            binding.attributes.addView(bindingHeader.getRoot());
                            for (ProductAttribute item : (List<ProductAttribute>) row.getValue()) {
                                AttributesLayoutItemBinding bindingItems = DataBindingUtil.inflate(getLayoutInflater(), R.layout.attributes_layout_item, null, false);
                                bindingItems.setAttribute(item);
                                binding.attributes.addView(bindingItems.getRoot());
                            }
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "observers: " + e.getMessage());
                    }
                } else binding.setLoading(false);
            });
        }
    }

    void setupRecycler() {
        binding.productImage.setSliderAdapter(sliderAdapter);
        binding.productImage.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.productImage.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.productImage.setScrollTimeInMillis(5000);
        binding.productImage.setIndicatorEnabled(true);
        binding.productImage.setIndicatorRtlMode(RtlMode.On);
        binding.productImage.startAutoCycle();
    }

    void invisibleContainers(Product product){
        if (product.getProduct_tag().size() > 0 )
            binding.tagsContainer.setVisibility(View.VISIBLE);
        else binding.tagsContainer.setVisibility(View.GONE);

        if (!product.getProduct_description().equals("") && !product.getProduct_short_description().equals(""))
            binding.desc.setVisibility(View.VISIBLE);
        else binding.desc.setVisibility(View.GONE);

        if (product.getProduct_supplier() != null)
            binding.productSeller.setVisibility(View.VISIBLE);
        else binding.productSeller.setVisibility(View.GONE);

        if (product.getProduct_attributes().size() > 0)
            binding.productsAttr.setVisibility(View.VISIBLE);
        else binding.productsAttr.setVisibility(View.GONE);
    }
}