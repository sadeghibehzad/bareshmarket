package com.baran.bareshmarket.ui.basketfrag;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

public class BasketViewModel extends ViewModel {

    private static final String TAG = "BasketViewModel";

    @Inject
    public BasketViewModel() {
        Log.d(TAG, "BasketViewModel: ");
    }
}
