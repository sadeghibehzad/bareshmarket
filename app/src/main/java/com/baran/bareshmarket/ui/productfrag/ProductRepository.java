package com.baran.bareshmarket.ui.productfrag;

import androidx.lifecycle.MutableLiveData;

import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.SingleProductResponse;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProductRepository {

    private static final String TAG = "ProductRepository";
    private MutableLiveData<Resource<Product>> productResult = new MutableLiveData<>();
    private RetroApi retroApi;

    @Inject
    public ProductRepository(RetroApi retroApi) {
        this.retroApi = retroApi;
    }

    void getProductById(int id){
        retroApi.getProductById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SingleProductResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        productResult.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull SingleProductResponse singleProductResponse) {
                        productResult.setValue(Resource.success(singleProductResponse.getData(), singleProductResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        productResult.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public MutableLiveData<Resource<Product>> getProductResult() {
        return productResult;
    }
}
