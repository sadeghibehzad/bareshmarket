package com.baran.bareshmarket.ui.settingfrag;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.FragmentBasketBinding;
import com.baran.bareshmarket.databinding.FragmentSettingBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.ui.basketfrag.BasketViewModel;
import com.baran.bareshmarket.ui.main.MainActViewModel;

import javax.inject.Inject;

public class SettingFragment extends BaseFragment {

    private static final String TAG = "SettingFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    private FragmentSettingBinding binding;
    private SettingViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this,providerFactory).get(SettingViewModel.class);
    }
}