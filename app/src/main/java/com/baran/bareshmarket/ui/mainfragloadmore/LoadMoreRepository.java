package com.baran.bareshmarket.ui.mainfragloadmore;

import androidx.lifecycle.MutableLiveData;

import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.repository.api.RetroApi;
import com.baran.bareshmarket.util.Resource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoadMoreRepository {

    private RetroApi retroApi;
    private MutableLiveData<Resource<ProductResponse>> productsResult = new MutableLiveData<>();

    @Inject
    public LoadMoreRepository(RetroApi retroApi) {
        this.retroApi = retroApi;
    }

    void getLatestProducts(int page){
        retroApi.getLastProducts(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        productsResult.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull ProductResponse productResponse) {
                        productsResult.setValue(Resource.success(productResponse, productResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        productsResult.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    void getBestSellerProducts(int page){
        retroApi.getBestSellingProducts(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        productsResult.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onNext(@NonNull ProductResponse productResponse) {
                        productsResult.setValue(Resource.success(productResponse, productResponse.getMessage()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        productsResult.setValue(Resource.error(e.getMessage(), null));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public MutableLiveData<Resource<ProductResponse>> getProductsResult() {
        return productsResult;
    }
}
