package com.baran.bareshmarket.ui.mainfrag;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.baran.bareshmarket.models.mianpage.ProductsCombinedRequest;
import com.baran.bareshmarket.util.Resource;

import javax.inject.Inject;

public class MainFragViewModel extends ViewModel {

    private LiveData<Resource<ProductsCombinedRequest>> mainRequests = new MutableLiveData<>();
    private MainFragRepository repository;

    @Inject
    public MainFragViewModel(MainFragRepository repository) {
        this.repository = repository;
        this.mainRequests = repository.getMainRequests();
        getData();
    }

    void getData(){
        repository.getLatestAndBest();
    }

    public LiveData<Resource<ProductsCombinedRequest>> getMainRequests() {
        return mainRequests;
    }
}
