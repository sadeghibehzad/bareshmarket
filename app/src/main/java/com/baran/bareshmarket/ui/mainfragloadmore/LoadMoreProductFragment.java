package com.baran.bareshmarket.ui.mainfragloadmore;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.baran.bareshmarket.BaseFragment;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.adapters.categories.SingleCategoryProductsAdapter;
import com.baran.bareshmarket.databinding.FragmentLoadMoreProductBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductResponse;
import com.baran.bareshmarket.util.Resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class LoadMoreProductFragment extends BaseFragment {

    private static final String TAG = "LoadMoreProductFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    @Inject
    SingleCategoryProductsAdapter singleCategoryProductsAdapter;
    private FragmentLoadMoreProductBinding binding;
    private LoadMoreViewModel viewModel;
    private String type = "latest";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_load_more_product, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, providerFactory).get(LoadMoreViewModel.class);
        if (viewModel.getProducts().getValue().size() == 0){
            viewModel.setCurrentPage(1);
            viewModel.setTotalPage(1);
        }
        binding.setLoading(true);
        binding.setLoadMore(false);
        setupRecycler();
        observers();

        binding.scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            try {
                View view1 = binding.scrollView.getChildAt(binding.scrollView.getChildCount() - 1);
                int diff = (view1.getBottom() - (binding.scrollView.getHeight() + binding.scrollView.getScrollY()));
                if (diff == 0)
                    if (viewModel.getCurrentPage().getValue() <= viewModel.getTotalPage().getValue()) {
                        binding.setLoadMore(true);
                        getData();
                    } else
                        Toast.makeText(getContext(), "داده ای برای بارگذاری موجود نیست", Toast.LENGTH_LONG).show();
            }catch (Exception e){}
        });

        singleCategoryProductsAdapter.setOnProductClickListener((product, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("product_id", product.getProduct_id());
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_loadMoreProductFragment_to_productFragment, bundle);
        });
    }

    void getData(){
        if (type.equals("latest")) {
            viewModel.getLatestProducts(viewModel.getCurrentPage().getValue());
            binding.subCatProductsText.setText("جدیدترین محصولات");
        } else {
            viewModel.getBestSellerProducts(viewModel.getCurrentPage().getValue());
            binding.subCatProductsText.setText("پرفروشترین محصولات");
        }
    }

    void observers() {
        if (!viewModel.getProductsResult().hasActiveObservers()){
            viewModel.getProductsResult().observe(getViewLifecycleOwner(), new Observer<Resource<ProductResponse>>() {
                @Override
                public void onChanged(Resource<ProductResponse> productResponseResource) {
                    if (productResponseResource.status == Resource.Status.SUCCESS){
                        setLoadingsFalse();
                        viewModel.setCurrentPage(productResponseResource.data.getPages().getCurrentPage() + 1);
                        viewModel.setTotalPage(productResponseResource.data.getPages().getLastPage());
                        viewModel.setProducts(productResponseResource.data.getData());
                    }
                }
            });
        }

        if (!viewModel.getProducts().hasActiveObservers()){
            viewModel.getProducts().observe(getViewLifecycleOwner(), new Observer<List<Product>>() {
                @Override
                public void onChanged(List<Product> products) {
                    if (products.size() == 0)
                        getData();
                    else {
                        setLoadingsFalse();
                        singleCategoryProductsAdapter.setData(products);
                    }
                }
            });
        }
    }

    void setupRecycler() {
        binding.catProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.catProducts.setAdapter(singleCategoryProductsAdapter);
    }

    void setLoadingsFalse() {
        binding.setLoading(false);
        binding.setLoadMore(false);
    }

}