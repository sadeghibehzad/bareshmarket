package com.baran.bareshmarket.ui.main;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.baran.bareshmarket.BaseActivity;
import com.baran.bareshmarket.BuildConfig;
import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.ActivityMainBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.ui.mainfrag.MainFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    private static final String TAG = "MainActivity";
    @Inject
    ViewModelProviderFactory providerFactory;
    private ActivityMainBinding binding;
    private MainActViewModel viewModel;
    private NavHostFragment navHostFragment;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this, providerFactory).get(MainActViewModel.class);

        binding.version.setText("نسخه: " + BuildConfig.VERSION_NAME);
        setUpNavigation();
    }

    public void setUpNavigation() {
        navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navController = navHostFragment.getNavController();
        NavigationUI.setupWithNavController(binding.bottomNav, navController);

        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            String s =destination.getLabel().toString();
           if (s.contains("MainFragment") || s.contains("CategoriesFragment") || s.contains("BasketFragment") || s.contains("ProfileFragment"))
               setSearchHint("جستجو در محصولات ...");
        });
    }

    public void setSearchHint(String title){
        binding.searchBar.setHint(title);
    }

    public void setSplashVisibility(boolean visibility){
        if (visibility)
            binding.splash.setVisibility(View.VISIBLE);
        else binding.splash.setVisibility(View.GONE);
    }

    public TextView getReload(){
        return binding.reload;
    }

    public void setReloadVisibility(boolean visibility){
       if (visibility)
           binding.reload.setVisibility(View.VISIBLE);
       else binding.reload.setVisibility(View.GONE);
    }
}