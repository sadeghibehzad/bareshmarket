package com.baran.bareshmarket.ui.signupfrag.confrimfrag;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.FragmentConfirmBinding;
import com.baran.bareshmarket.databinding.FragmentPhoneBinding;
import com.baran.bareshmarket.di.viewmodels.ViewModelProviderFactory;
import com.baran.bareshmarket.ui.signupfrag.phonefrag.PhoneViewModel;

import javax.inject.Inject;

public class ConfirmFragment extends Fragment {

    private static final String TAG = "ConfirmFragment";
    @Inject
    ViewModelProviderFactory providerFactory;
    private FragmentConfirmBinding binding;
    private ConfirmViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this, providerFactory).get(ConfirmViewModel.class);

    }
}