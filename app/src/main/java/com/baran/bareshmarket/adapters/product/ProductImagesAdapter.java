package com.baran.bareshmarket.adapters.product;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.ProductImagePrevBinding;
import com.baran.bareshmarket.models.product.api.ProductImage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ProductImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<ProductImage> images;
    private OnImageClickListener onImageClickListener;

    @Inject
    public ProductImagesAdapter() {
        images = new ArrayList<>();
    }

    public void setData(List<ProductImage> images){
        this.images = images;
        notifyDataSetChanged();
    }

    public void setOnImageClickListener(OnImageClickListener onImageClickListener){
        this.onImageClickListener = onImageClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProductImagePrevBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.product_image_prev, parent, false);
        return new ProductImageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ProductImageViewHolder) holder).bind(images.get(position));
        if (onImageClickListener != null)
            ((ProductImageViewHolder) holder).itemView.setOnClickListener(view -> onImageClickListener.onImageClicked(images.get(position), position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ProductImageViewHolder  extends RecyclerView.ViewHolder{
        private final ProductImagePrevBinding binding;
        public ProductImageViewHolder(@NonNull ProductImagePrevBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ProductImage image){
            binding.setImage(image);
        }
    }

    public interface OnImageClickListener{
        void onImageClicked(ProductImage image, int position);
    }
}
