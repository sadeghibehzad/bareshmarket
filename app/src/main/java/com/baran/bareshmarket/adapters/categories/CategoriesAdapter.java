package com.baran.bareshmarket.adapters.categories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.CategoriesLayoutBinding;
import com.baran.bareshmarket.models.category.api.Category;
import com.baran.bareshmarket.models.product.api.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Category> categories;
    private OnShowMoreClick onShowMoreClick;

    @Inject
    public CategoriesAdapter() {
        categories = new ArrayList<>();
    }

    public void setData(List<Category> categories){
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void setShowMoreListener(OnShowMoreClick onShowMoreClick){
        this.onShowMoreClick = onShowMoreClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CategoriesLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.categories_layout, parent, false);
        return new CategoriesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CategoriesViewHolder) holder).bind(categories.get(position));
        CategoriesProductAdapter categoriesProductAdapter = new CategoriesProductAdapter();
        ((CategoriesViewHolder) holder).binding.catProducts.setLayoutManager(
                new LinearLayoutManager(holder.itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        ((CategoriesViewHolder) holder).binding.catProducts.setAdapter(categoriesProductAdapter);
        categoriesProductAdapter.setData(categories.get(position).getCategory_products().getData());
        categoriesProductAdapter.setOnProductClickListener((product, position1) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("product_id", product.getProduct_id());
            Navigation.findNavController(holder.itemView).navigate(R.id.action_categoriesFragment_to_productFragment, bundle);
        });
        if (onShowMoreClick != null)
            ((CategoriesViewHolder) holder).binding.catMore.setOnClickListener(v -> onShowMoreClick.onShowMoreClicked(position, categories.get(position)));

    }

    @Override
    public int getItemCount() {
        if (categories == null)
            return 0;
        else return categories.size();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder{

        private final CategoriesLayoutBinding binding;

        public CategoriesViewHolder(@NonNull CategoriesLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Category category){
            binding.setCategory(category);
            binding.executePendingBindings();
        }
    }

    public interface OnShowMoreClick{
        void onShowMoreClicked(int position, Category category);
    }
}
