package com.baran.bareshmarket.adapters.categories;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.SingleCategoryLayoutBinding;
import com.baran.bareshmarket.models.category.api.Category;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SingleCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Category> categories;
    private OnCategoryClick onCategoryClick;

    @Inject
    public SingleCategoryAdapter() {
        categories = new ArrayList<>();
    }

    public void setData(List<Category> categories){
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void setCatListener(OnCategoryClick onCategoryClick){
        this.onCategoryClick = onCategoryClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SingleCategoryLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_category_layout, parent, false);
        return new SingleCategoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SingleCategoryViewHolder) holder).bind(categories.get(position));
        if (onCategoryClick != null)
            ((SingleCategoryViewHolder) holder).itemView.setOnClickListener(v -> onCategoryClick.onCategoryClicked(position, categories.get(position)));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    static class SingleCategoryViewHolder extends RecyclerView.ViewHolder{

        private final SingleCategoryLayoutBinding binding;

        public SingleCategoryViewHolder(@NonNull SingleCategoryLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Category category){
            binding.setCategory(category);
            binding.executePendingBindings();
        }
    }

    public interface OnCategoryClick{
        void onCategoryClicked(int position, Category category);
    }
}
