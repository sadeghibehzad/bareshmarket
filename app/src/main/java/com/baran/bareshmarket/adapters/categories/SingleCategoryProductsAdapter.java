package com.baran.bareshmarket.adapters.categories;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.SingleCatgoryProductsBinding;
import com.baran.bareshmarket.models.product.api.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SingleCategoryProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> products;
    private OnProductClickListener onProductClickListener;

    @Inject
    public SingleCategoryProductsAdapter() {
        this.products = new ArrayList<>();
    }

    public void setData(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void addProducts(List<Product> products) {
        if (!this.products.containsAll(products))
            this.products.addAll(products);
        notifyDataSetChanged();
    }

    public void setOnProductClickListener(OnProductClickListener onProductClickListener) {
        this.onProductClickListener = onProductClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SingleCatgoryProductsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.single_catgory_products, parent, false);
        return new SingleCatProducts(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SingleCatProducts) holder).bind(products.get(position));
        if (onProductClickListener != null)
            ((SingleCatProducts) holder).itemView.setOnClickListener(view -> onProductClickListener.onProductClicked(products.get(position), position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public interface OnProductClickListener {
        void onProductClicked(Product product, int position);
    }

    static class SingleCatProducts extends RecyclerView.ViewHolder {
        private final SingleCatgoryProductsBinding binding;

        public SingleCatProducts(@NonNull SingleCatgoryProductsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Product product) {
            binding.setProducts(product);
            binding.executePendingBindings();
        }
    }
}
