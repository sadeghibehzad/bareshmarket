package com.baran.bareshmarket.adapters.categories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.CatProductLayoutBinding;
import com.baran.bareshmarket.databinding.CategoriesLayoutBinding;
import com.baran.bareshmarket.models.product.api.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class CategoriesProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> products;
    private OnProductClickListener onProductClickListener;

    @Inject
    public CategoriesProductAdapter() {
        products = new ArrayList<>();
    }

    public void setData(List<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }

    public void setOnProductClickListener(OnProductClickListener listener){
        this.onProductClickListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CatProductLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.cat_product_layout, parent, false);
        return new CategoriesProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CategoriesProductViewHolder) holder).bind(products.get(position));
        if (onProductClickListener != null)
            ((CategoriesProductViewHolder) holder).itemView.setOnClickListener( view -> onProductClickListener.onProductClicked(products.get(position), position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class CategoriesProductViewHolder extends RecyclerView.ViewHolder{
        private final CatProductLayoutBinding binding;

        public CategoriesProductViewHolder(@NonNull CatProductLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Product product){
            binding.setProduct(product);
            binding.executePendingBindings();
        }
    }

    public interface OnProductClickListener{
        void onProductClicked(Product product, int position);
    }
}
