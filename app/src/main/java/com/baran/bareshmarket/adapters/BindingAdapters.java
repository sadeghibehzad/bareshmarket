package com.baran.bareshmarket.adapters;

import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.models.product.api.Product;
import com.baran.bareshmarket.models.product.api.ProductImage;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

public class BindingAdapters {

    @BindingAdapter("app:src")
    public static void loadImage(ImageView view, String url) {
        if (url == null)
            view.setImageResource(R.mipmap.ic_launcher);
        else
            Glide.with(view.getContext()).load(url).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(view);
    }

    @BindingAdapter("app:src")
    public static void loadImage(RoundedImageView view, List<ProductImage> images) {
        if (images.size() > 0) {
            if (images.get(0).getImage_path() == null) {
                view.setImageResource(R.mipmap.ic_launcher);
            } else {
                Glide.with(view.getContext()).load(images.get(0).getImage_path()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(view);
            }
        }
    }

    @BindingAdapter("app:singleProduct")
    public static void loadSingleImage(RoundedImageView view, List<ProductImage> images) {
        if (images != null) {
            if (images.get(0).getImage_path() == null)
                view.setImageResource(R.mipmap.ic_launcher);
            else
                Glide.with(view.getContext()).load(images.get(0).getImage_path()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(view);
        }
    }

    @BindingAdapter("app:src")
    public static void loadSingleImage(RoundedImageView view, ProductImage image) {
        if (image.getImage_path() == null)
            view.setImageResource(R.mipmap.ic_launcher);
        else
            Glide.with(view.getContext()).load(image.getImage_path()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(view);
    }

    @BindingAdapter("app:exist")
    public static void productExist(TextView view, Product product) {
        if (product.isProduct_is_exist()) {
            view.setText("محصول موجود است");
        }
    }

    @BindingAdapter("app:textFromHtml")
    public static void productDescFromHtml(TextView view, Product product) {
        if (product != null)
            view.setText(Html.fromHtml(product.getProduct_description().equals("") ? product.getProduct_short_description() : product.getProduct_description()));
    }

}
