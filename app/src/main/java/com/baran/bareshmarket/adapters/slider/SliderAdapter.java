package com.baran.bareshmarket.adapters.slider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.baran.bareshmarket.R;
import com.baran.bareshmarket.databinding.SliderLayoutBinding;
import com.baran.bareshmarket.models.slider.api.Slider;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private List<Slider> mSliderItems = new ArrayList<>();
    private OnSliderClickListener onSliderClickListener;
    @Inject
    public SliderAdapter() {
    }

    public void renewItems(List<Slider> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(Slider sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    public void setListener(OnSliderClickListener onSliderClickListener){
        this.onSliderClickListener = onSliderClickListener;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        SliderLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.slider_layout, parent, false);
        return new SliderAdapterVH(binding);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        Slider sliderItem = mSliderItems.get(position);
        ((SliderAdapterVH) viewHolder).bind(sliderItem);
        if (onSliderClickListener != null)
            ((SliderAdapterVH) viewHolder).itemView.setOnClickListener((View.OnClickListener) v -> onSliderClickListener.onSlideClicked(sliderItem, position));
    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        private final SliderLayoutBinding binding;

        public SliderAdapterVH(SliderLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Slider slider) {
            binding.setSlide(slider);
            binding.executePendingBindings();
        }
    }

    public interface OnSliderClickListener{
        void onSlideClicked(Slider slider, int position);
    }

}